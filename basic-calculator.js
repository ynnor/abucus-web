

class Calculator {
    constructor(){
        this.currentInputValue = '';
        this.previousInputValue = '';
        this.storedOperation = '';
        this.solvedDiplaying = false;
        this.historyCount = 0;
        this.createOnClickListeners();
    }

    createOnClickListeners(){
        this.numberButtons.forEach(button => button.addEventListener('click', () => this.appendValue(button.innerText)));
        this.operationButtons.forEach(button => button.addEventListener('click', () => this.operate(button.innerText)));
        this.decimalPointButton.addEventListener('click', () => this.addDecimal());
        this.clearButton.addEventListener('click', () => this.clear());
        this.backspaceButton.addEventListener('click', () => this.backspace());
        this.plusOrMinusButton.addEventListener('click', () => this.toggleNegative());
    }

    appendValue(value){
        if (this.currentInputValue.length == 22) return;
        if (this.solvedDiplaying == true){
            this.clear();
        }
        if (this.currentInputValue == '0' && value != '.') this.currentInputValue = '';
        

        this.currentInputValue += value;
        this.formatDisplay();
    }

    operate(operation){
        if (this.currentInputValue.length == 0 && operation != '=' && this.previousInputValue.length > 0) {
            this.previousInputValue = this.previousInputValue.slice(0,-1) + operation;
            this.storedOperation = operation;
            this.formatDisplay();
            return;
        }

        if (this.currentInputValue.length == 0) return;

        if (operation == '=' && this.previousInputValue.length == 0) return;

        if (this.storedOperation.length == 1){
            this.solve(operation);
            return;
        }
        this.previousInputValue = this.currentInputValue + ' ' + operation;
        this.currentInputValue = '';
        this.storedOperation = operation;
        this.solvedDiplaying = false;
        this.formatDisplay();
    }

    solve(operation) {
        if (this.currentInputValue.length == 0) return;
        let result = 0;

        console.log('stored:' + this.storedOperation + 'current operation:' + operation);

        switch(this.storedOperation){
            case '+': 
                result = parseFloat(this.currentInputValue) + parseFloat(this.previousInputValue.slice(0,-2));
                break;

            case '−': 
                result = parseFloat(this.previousInputValue.slice(0,-2)) - parseFloat(this.currentInputValue);
                break
            case '×': 
                result = parseFloat(this.currentInputValue) * parseFloat(this.previousInputValue.slice(0,-2));
                break;
            case '/':;
                result = parseFloat(this.previousInputValue.slice(0,-2)) / parseFloat(this.currentInputValue);
                break;
            default: console.log('something went wrong');
        }
        console.log('result: ' + result);
        this.addToHistory(result);


        if (operation == '='){
            this.previousInputValue = ''
            this.currentInputValue = result.toString();
            this.storedOperation = '';
            this.formatDisplay();
            this.solvedDiplaying = true;
        }
        else {
            this.previousInputValue = result + ' ' + operation;
            this.currentInputValue = '';
            this.storedOperation = operation;
            this.formatDisplay();
        }

    }

    addDecimal(){
        if (this.currentInputValue.includes('.') == false) this.appendValue('.');
    }

    toggleNegative(){
        if (this.currentInputValue.includes('-')){
            this.currentInputValue = this.currentInputValue.slice(1);
            
        }
        else this.currentInputValue = '-' + this.currentInputValue;

        this.formatDisplay();
        this.solvedDiplaying = false;
    }

    clear(){
        this.currentInputValue = '';
        this.previousInputValue = '';
        this.storedOperation = '';
        this.formatDisplay();
        this.solvedDiplaying = false;
    }
    backspace() {
        if (this.currentInputValue.length == 0 && this.previousInputValue.length > 0 ){
            this.currentInputValue = this.previousInputValue.slice(0, -2);
            this.previousInputValue = '';
            this.storedOperation = '';
            this.formatDisplay();
            
            return;
        }

        this.currentInputValue = this.currentInputValue.slice(0,-1);
        this.formatDisplay();
        this.solvedDiplaying = false;
    }


    formatDisplay(){
        this.currentInputText.innerText = this.currentInputValue;
        this.previousInputText.innerText = this.previousInputValue;
    }

    addToHistory(result){
        let historyTable = document.getElementById('history-table');
        let historyRow = document.createElement('div');
        historyRow.classList.add('history-row');
        historyRow.innerHTML = `
        
        <div>
            <div class="history-equation">${this.previousInputValue.slice(0,-2)} ${this.storedOperation} ${this.currentInputValue}</div>
            <div class="history-result"> ${result}</div>
        </div>
        `;
        
        historyTable.insertBefore(historyRow, historyTable.firstChild);

        if (this.historyCount != 7) this.historyCount++;
        if (this.historyCount == 7) historyTable.removeChild(historyTable.lastChild);
        
    }
    
    numberButtons =         document.querySelector('.calculator-grid').querySelectorAll(' .number-button');
    operationButtons =      document.querySelector('.calculator-grid').querySelectorAll('.operation-button');
    
    decimalPointButton =    document.querySelector('.calculator-grid').querySelector('#decimal-point-button');
    clearButton =           document.querySelector('.calculator-grid').querySelector('#clear-button');
    previousInputText =     document.querySelector('.calculator-section').querySelector('.previous-input');
    currentInputText =      document.querySelector('.calculator-section').querySelector('.current-input');
    backspaceButton =       document.querySelector('.calculator-grid').querySelector('#backspace-button');
    plusOrMinusButton =     document.querySelector('.calculator-grid').querySelector('#plus-or-minus-button');
}
const calculator = new Calculator();
