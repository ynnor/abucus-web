class Token {
    constructor(type, value, precedence){
        this.type = type;
        this.value = value;
        
        if (precedence === undefined) this.precedence = 0;
        else this.precedence = precedence
    }
}

class ScientificCalculator {
    constructor(){
        this.createOnClickListeners();
        this.tokens = [];
        this.radiansMode = true;
        this.numberOffLeftParentheses = 0;
        this.numberOfRightParenthese = 0;
        this.solutionDisplaying = false;
    }
    createOnClickListeners(){
        this.numberButtons.forEach(button => button.addEventListener('click', () => this.appendNumber(button.innerText)));
        this.operationButtons.forEach(button => button.addEventListener('click', () => this.appendOperation(button.innerText)));
        this.modeButtons.forEach(button => button.addEventListener('click', () => this.toggleMode(button.innerText)));
        this.parenthesesButtons.forEach(button => button.addEventListener('click', () => this.appendParenthesis(button.innerText)));
        this.constantsButtons.forEach(button => button.addEventListener('click', () => this.appendConstant(button.innerText)));
        this.functionButtons.forEach(button => button.addEventListener('click', () => this.appendFunction(button.innerText)));

        this.backspaceButton.addEventListener('click', () => this.backspace());
        this.plusOrMinusButton.addEventListener('click', () => this.toggleNegative());
        this.clearButton.addEventListener('click', () => this.clear());
        this.decimalPointButton.addEventListener('click', () => this.appendDecimal());
        this.upCaretButton.addEventListener('click', () => this.appendCaret());
    }
    appendNumber(value){
        if (this.solutionDisplaying){
            this.tokens = [];
            this.solutionDisplaying = false;

            if (parseFloat(value) < 0){
                this.toggleNegative();
                value = value.slice(1);
            }
        }

        if (this.tokens.length == 0){
            this.tokens.push(new Token('number', value));
            this.display();
            return;
        }

        let lastToken = this.tokens[this.tokens.length - 1];
        if (lastToken.type == 'number'){
            lastToken.value = lastToken.value + value;
            this.display();
            return;
        }

        if (lastToken.value == ')') throw new Error('Incorrect Syntax');

        this.tokens.push(new Token('number', value));

        this.display();
    }

    appendOperation(operation){
        if (this.tokens.length == 0 && !this.solutionDisplaying) throw new Error('Incorrect Syntax');
        
        if (this.solutionDisplaying) this.appendNumber(this.currentInputText.innerText);

        if (operation == '='){
            this.solve();
            return;
        }

        let lastToken = this.tokens[this.tokens.length - 1];
        if (lastToken.type == 'operation' || lastToken.value == '(') throw new Error('Incorrect Syntax');
        
        let precedence;
        if (operation == '×' || operation == '/') precedence = 2;
        else precedence = 1;
        console.log('minus symbol:');
        console.log(operation);
        this.tokens.push(new Token('operation', operation, precedence));
        this.display();
    }

    toggleMode(mode){
        if (mode == 'Rad' && this.radiansMode == false) this.radiansMode = true;
        if (mode == 'Deg' && this.radiansMode == true) this.radiansMode = false;
    }

    appendParenthesis(parentheses) {
        if (parentheses == ')' && this.numberOffLeftParentheses == this.numberOfRightParenthese){
            throw new Error('Incorrect Syntax');
        }
        if (parentheses == ')'){
            let lastToken = this.tokens[this.tokens.length - 1];
            if (lastToken.type == 'operation') throw new Error('Incorrect Syntax');
        }
        this.tokens.push(new Token('parenthese', parentheses));

        if (parentheses == '(') this.numberOffLeftParentheses++;
        if (parentheses == ')') this.numberOfRightParenthese++;

        this.display();
    }

    appendConstant(constant){
        if (constant == 'ex'){
            this.tokens.push(new Token('constant', 'e'));
            this.appendCaret();
        }
        else {
            this.tokens.push(new Token('constant', constant));
        }
        this.display();
        
    }
    appendCaret(){ 
        if (this.solutionDisplaying) this.appendNumber(this.currentInputText.innerText);
        if (this.tokens.length == 0) throw new Error('Incorrect Syntax');

        let lastToken = this.tokens[this.tokens.length - 1];
        if (lastToken.value == '(' || lastToken.value == '^') throw new Error('Incorrect Syntax');

        this.tokens.push(new Token('exponent', '^', 3));
        this.appendParenthesis('(');
        this.display();
    }

    appendFunction(mathfunction){
        if (mathfunction == '| x |') mathfunction = 'abs';
        if (mathfunction == '√') mathfunction = 'sqrt';
        
        
        this.tokens.push(new Token('function', mathfunction, 3));
        this.appendParenthesis('(');
    }
    backspace(){
        if (this.solutionDisplaying){
            this.clear();
            return;
        }

        if (this.tokens.length == 0) return;

        if (this.tokens.length == 1){
            this.clear()
            return;
        }
        let removedToken = this.tokens.pop();
        let newLastToken = this.tokens[this.tokens.length - 1];

        if (removedToken.value == '('){
            if (newLastToken.type == 'function' || newLastToken.type == 'exponent') this.backspace();
            this.numberOffLeftParentheses--;
        }
        if (removedToken.value == '×' && newLastToken.value == '-1') this.tokens.pop();

        if (removedToken.value == ')') this.numberOfRightParenthese--;

        if (removedToken.type == 'exponent' && newLastToken.value == 'e') this.backspace();

        if (removedToken.type == 'number' && removedToken.value.length > 1) this.appendNumber(removedToken.value.slice(0,-1));

        this.display();
    }
    clear(){
        this.tokens = [];
        this.numberOffLeftParentheses = 0;
        this.numberOfRightParenthese = 0;
        this.previousInputText.innerText = '';
        this.solutionDisplaying == false;
        this.display();
    }

    toggleNegative(){
        if (this.tokens.length == 0){
            this.appendNumber('-1');
            this.appendOperation('×');
            return;
        }

        let lastToken = this.tokens[this.tokens.length - 1];
        if (lastToken.type == 'number'){
            let storedToken = this.tokens.pop();
            this.appendNumber('-1');
            this.appendOperation('×');
            this.tokens.push(storedToken);
            this.display();
            return;
        }

        this.appendNumber('-1');
        this.appendOperation('×');
    }

    appendDecimal(){
        if (this.tokens.length == 0){
            this.appendNumber('.');
            return;
        }

        let lastToken = this.tokens[this.tokens.length - 1];
        if (lastToken.value.includes('.') == false) this.appendNumber('.');
    }
    solve(){
        if (this.numberOfRightParenthese !== this.numberOffLeftParentheses) throw Error('Mismatched Parentheses');
        if (this.tokens.length == 1) return;
        if (this.tokens[this.tokens.length - 1].type == 'operation') throw Error('Incomplete Expression');
        if (this.solutionDisplaying) return;

        

        let expressionWithMultiplicstionSymbols = this.insertImplicitMultiplicationSymbols(this.tokens);
        let postfixExpression = this.convertInfixToPostfix(expressionWithMultiplicstionSymbols);
        let solution = this.solvePostFixExpression(postfixExpression);

        let previousExpression = this.formatDisplayText();
        this.clear();

        this.currentInputText.innerText = solution;
        this.previousInputText.innerText = previousExpression;
        this.solutionDisplaying = true;
        console.log(solution);
        
    }

    insertImplicitMultiplicationSymbols(expression){
        let tokens = expression.slice();
        for (let i = tokens.length - 1; i > 0; i--){
            let currentToken = tokens[i];
            let tokenBeforeCurrentToken = tokens[i - 1];
            if (currentToken.type == 'function' || currentToken.value == '('){
                if (tokenBeforeCurrentToken.type == 'number' || 
                    tokenBeforeCurrentToken.type == 'constant'||
                    tokenBeforeCurrentToken.value == ')'){
                    
                    tokens.splice( i , 0 , new Token('operation', '×' , 2));
                }
                
            }
            if (currentToken.type == 'number' && tokenBeforeCurrentToken.type == 'constant'){
                tokens.splice( i , 0 , new Token('operation', '×' , 2));
            }

            if (currentToken.type == 'constant'){
                if (tokenBeforeCurrentToken.type == 'number' || tokenBeforeCurrentToken.value == ')'){
                    tokens.splice( i , 0 , new Token('operation', '×' , 2));
                }
            }
        }
        return tokens;
     }

    convertInfixToPostfix(infixExpression){
        // Modify a copy of the array
        let tokens = infixExpression.slice();


        console.log(tokens);
        let operatorStack = [];
        let numberQueue = [];

        while (tokens.length != 0){
            
            let currentToken = tokens.shift();
            
            if (currentToken.type == 'number' || currentToken.type == 'constant') numberQueue.push(currentToken);
            if (currentToken.type == 'function') operatorStack.push(currentToken);
            if (currentToken.type == 'operation' || currentToken.type == 'exponent'){

                if (operatorStack.length == 0) operatorStack.push(currentToken);
                else {
                    let topOFOperatorStack = operatorStack[operatorStack.length - 1];
                    let matchingExponents = false;
                    if (topOFOperatorStack.type == 'exponent' && currentToken.type == 'exponent'){
                        matchingExponents = true;
                    }

                    while (operatorStack.length != 0 &&
                        topOFOperatorStack.value != '(' &&
                        !matchingExponents &&
                        (currentToken.precedence <= topOFOperatorStack.precedence)){

                            console.log('^ over x');
                            numberQueue.push(operatorStack.pop());
                            topOFOperatorStack = operatorStack[operatorStack.length -1];
                        }
                        operatorStack.push(currentToken);
                }
            }
            if (currentToken.value == '(') operatorStack.push(currentToken);

            if (currentToken.value == ')'){
                let topOFOperatorStack = operatorStack[operatorStack.length -1];
                while (topOFOperatorStack.value != '('){
                    numberQueue.push(operatorStack.pop());
                    topOFOperatorStack = operatorStack[operatorStack.length -1];
                }
                operatorStack.pop();
                
                if (operatorStack.length > 0){
                    topOFOperatorStack = operatorStack[operatorStack.length -1]
                    if (topOFOperatorStack.type == 'function') numberQueue.push(operatorStack.pop());
                }
            }
        }
        while (operatorStack.length != 0) numberQueue.push(operatorStack.pop());

        
        console.log(tokens);
        console.log(numberQueue);
        console.log(operatorStack);
        return numberQueue;
    }

    solvePostFixExpression(postFixExpression){
        let tokens = postFixExpression.slice();

        let numberStack = [];

        while (tokens.length > 0) {
            let currentToken = tokens.shift();

            switch (currentToken.value){
                case 'π':
                    currentToken.value = Math.PI.toString();
                    break;
                case 'e':
                    currentToken.value = Math.E.toString();
            }
            

            console.log(currentToken);

            if (currentToken.type == 'number' || currentToken.type == 'constant') numberStack.push(currentToken);
            
            else {
                if (currentToken.type == 'function'){
                    let operand = parseFloat(numberStack.pop().value);
                    let solution;

                    if (this.radiansMode == false){
                        operand = operand * (Math.PI / 180);
                    }

                    if (currentToken.value == 'sin') solution = Math.sin(operand);
                    if (currentToken.value == 'cos') solution = Math.cos(operand);
                    if (currentToken.value == 'tan') solution = Math.tan(operand);
                    if (currentToken.value == 'arcsin') solution = Math.asin(operand);
                    if (currentToken.value == 'arccos') solution = Math.acos(operand);
                    if (currentToken.value == 'arctan') solution = Math.atan(operand);
                    if (currentToken.value == 'sinh') solution = Math.sinh(operand);
                    if (currentToken.value == 'cosh') solution = Math.cosh(operand);
                    if (currentToken.value == 'tanh') solution = Math.tanh(operand);
                    if (currentToken.value == 'sqrt') solution = Math.sqrt(operand);
                    if (currentToken.value == 'abs') solution = Math.abs(operand);
                    if (currentToken.value == 'ln') solution = Math.log(operand);
                    if (currentToken.value == 'log') solution = Math.log(operand) / Math.log(10);
                    

                    let token = new Token('number', solution.toString());
                    numberStack.push(token);
                }
                if (currentToken.type == 'exponent'){
                    let exponent = parseFloat(numberStack.pop().value);
                    let base = parseFloat(numberStack.pop().value);
                    let solution = Math.pow(base , exponent);
                    let token = new Token('number', solution.toString());
                    numberStack.push(token);
                }
                if (currentToken.type == 'operation'){
                    let rightNumber = parseFloat(numberStack.pop().value);
                    let leftNumber = parseFloat(numberStack.pop().value);
                    console.log('left: ' + leftNumber);
                    console.log('right: ' + rightNumber);
                    let solution;

                    if (currentToken.value == '+') solution = leftNumber + rightNumber;
                    if (currentToken.value == '−') solution = leftNumber - rightNumber;
                    if (currentToken.value == '×') solution = leftNumber * rightNumber;
                    if (currentToken.value == '/') solution = leftNumber / rightNumber;

                    console.log('solution: ' + solution);
                    console.log('string solution' + solution.toString());
                    let token = new Token('number', solution.toString());
                    numberStack.push(token);
                }
            }
        }
        return parseFloat(numberStack.pop().value);
    }

    display(){

        this.currentInputText.innerText = this.formatDisplayText();
    }

    formatDisplayText(){
        let text = ''
        for (let i = 0; i < this.tokens.length; i++){
            if (this.tokens[i].value == '-1' && i+1 != this.tokens.length){
                if (this.tokens[i + 1].value == '×'){
                    text = text + '-';
                    i++;
                }
            }
            else if (this.tokens[i].type == 'function' && i != 0){
                if (this.tokens[i-1].type == 'number'){
                    text = text.slice(0, -1) + this.tokens[i].value;
                }else text = text + this.tokens[i].value + ' ';
            }
            else text = text + this.tokens[i].value + ' ';
        }
        return text;
    }

    currentInputText =      document.querySelector('.sc-current-input');
    previousInputText =     document.querySelector('.sc-previous-input');


    numberButtons =         document.querySelector('.calculator-grid').querySelectorAll(' .number-button');
    numberButtons =         document.querySelector('.scientific-grid').querySelectorAll('.sc-number-button');
    operationButtons =      document.querySelector('.scientific-grid').querySelectorAll('.sc-operation-button');
    modeButtons =           document.querySelector('.scientific-grid').querySelectorAll('.mode-button');
    parenthesesButtons =    document.querySelector('.scientific-grid').querySelectorAll('.parentheses');
    constantsButtons =      document.querySelector('.scientific-grid').querySelectorAll('.math-constant');
    functionButtons =       document.querySelector('.scientific-grid').querySelectorAll('.math-function');


    backspaceButton =       document.querySelector('.scientific-grid').querySelector('#sc-backspace-button');
    plusOrMinusButton =     document.querySelector('.scientific-grid').querySelector('#sc-plus-or-minus-button');
    clearButton =           document.querySelector('.scientific-grid').querySelector('#sc-clear-button');
    upCaretButton =         document.querySelector('.scientific-grid').querySelector('#up-caret');
    decimalPointButton =    document.querySelector('.scientific-grid').querySelector('#sc-decimal-point-button');
}

test = document.querySelectorAll('.math-function');
test.forEach(button => console.log(button.innerText));

mycalc = new ScientificCalculator();
